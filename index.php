<?php
// Boolean

$boolTrue = true;
$boolFalse = false;
$string = 'string';
$integer = 'integer';

$action = "123"; // String

    echo '<br>';

if ($action == "123") {
    echo "$string. ==Перевірка пройшла вдало";
}
    echo '<br>';

if ($action == "123") {
    echo "$integer. ==Перевірка пройшла вдало";
}
    echo '<br>';

// Перевірка чи це булеве значення
var_dump(is_bool(var:$action === 123));
var_dump(boolval($action));
var_dump((bool) $action);
var_dump((boolean) $action);